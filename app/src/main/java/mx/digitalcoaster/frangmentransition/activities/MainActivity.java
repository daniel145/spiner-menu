package mx.digitalcoaster.frangmentransition.activities;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;


import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;

import mx.digitalcoaster.frangmentransition.R;
import mx.digitalcoaster.frangmentransition.TouchImageView;
import mx.digitalcoaster.frangmentransition.activities.ExampleDrawView;


/**
 * Demonstrates the use of custom animations in a FragmentTransaction when
 * pushing and popping a stack.
 */
public class MainActivity extends Activity {

    ExampleDrawView draw;


    private static final float ROTATE_TIME_MILLIS = 2000 ;
    private TouchImageView image;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        draw = findViewById(R.id.iv_photo);
        Button btn_rotate = (Button) findViewById(R.id.button);
        Button btn_rotate2 = (Button) findViewById(R.id.button2);
        Button btn_rotate3 = (Button) findViewById(R.id.button3);

        btn_rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                draw.startDrawing();

            }
        });

        btn_rotate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                draw.startDrawing2();

            }
        });

        btn_rotate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up,R.anim.slide_no_change);

            }
        });

        Matrix matrix = new Matrix();
        /*
        image.setScaleType(TouchImageView.ScaleType.MATRIX);   //required
        matrix.postRotate((float) 360, 50, 100);
        image.setImageMatrix(matrix);*/


    }

    public void doCanvas(){
        //Create our resources
        Drawable bit = ContextCompat.getDrawable(this, R.drawable.wheel);
        Bitmap bitmap = Bitmap.createBitmap(1000, 1000, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        final Bitmap chefBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.wheel);

        //Link the canvas to our ImageView
        image.setImageBitmap(bitmap);

        ValueAnimator animation= ValueAnimator.ofFloat(0, 359, 129, 186);
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (Float) animation.getAnimatedValue();
                //Clear the canvas
                canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                canvas.save();
                canvas.rotate(value, canvas.getWidth()/2, canvas.getHeight()/2);
                canvas.drawBitmap(chefBitmap, 0, 0, null);
                canvas.restore();
                image.invalidate();
            }
        });
        animation.setInterpolator(new LinearInterpolator());
        animation.setDuration(10000);
        animation.start();
    }


}

