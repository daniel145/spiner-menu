package mx.digitalcoaster.frangmentransition.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

import mx.digitalcoaster.frangmentransition.R;

public class ExampleDrawView extends View {

    Bitmap bitmap;
    Float mRotate = 0f;
    Float mAux = 0f;
    Handler h;
    int height;
    int width;
    //State variables
    final int STATE_PAUSE = 2;
    final int STATE_ROTATE = 3;
    final int STATE_ROTATE_LEFT = 4;
    int STATE_CURRENT;

    public ExampleDrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        h = new Handler();
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rueda);
        height = bitmap.getHeight();
        width = bitmap.getWidth();
        STATE_CURRENT = STATE_PAUSE;

    }

    Runnable move = new Runnable() {
        @Override
        public void run() {
            switch (STATE_CURRENT) {
                case STATE_ROTATE:
                    if (mAux < 42) {
                        mAux++;
                        mRotate++;
                        invalidate();
                    } else {
                        STATE_CURRENT = STATE_PAUSE;
                    }
                    h.postDelayed(move, 1);
                    break;
            }
        }
    };

    Runnable move2 = new Runnable() {
        @Override
        public void run() {
            switch (STATE_CURRENT) {
                case STATE_ROTATE:
                    if (mAux > -42) {
                        mAux--;
                        mRotate--;
                        invalidate();
                    } else {
                        STATE_CURRENT = STATE_PAUSE;
                    }
                    h.postDelayed(move2, 1);
                    break;
            }
        }
    };

    public void startDrawing() {
        if (STATE_CURRENT == STATE_PAUSE) {
            STATE_CURRENT = STATE_ROTATE;
            mAux = (float) 0;
            h.postDelayed(move, 1);
        }
    }

    public void startDrawing2() {
        if (STATE_CURRENT == STATE_PAUSE) {
            STATE_CURRENT = STATE_ROTATE;
            mAux = (float) 0;
            h.postDelayed(move2, 1  );
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);
        //change your rotate point here, i just made it the middle of the canvas
        canvas.rotate(mRotate, getWidth()/2, height/2 + 400);
        canvas.drawBitmap(bitmap,  -490, 400, null);

        //canvas.drawBitmap(bitmap,  -30, getHeight() - (bitmap.getHeight()/2), null);
    }


}
