package mx.digitalcoaster.frangmentransition.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import mx.digitalcoaster.frangmentransition.R;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
}
